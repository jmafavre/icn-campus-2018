# Conception de cartes en relief pour déficients visuels

[ACTIVmap](http://activmap.limos.fr) est un projet de recherche mené au LIMOS. Nous y travaillons à la conception d'outils pour l'aide à la génération de cartes en relief pour déficients visuels. On peut retrouver sur le site [une vidéo expliquant le projet](https://activmap.limos.fr/Activites/Diffusion/).

Dans ces travaux, nous nous intéressons à la question de la simplification du tracé des rues, afin de rendre leur forme compréhensible au toucher. Pour cela, il faut simplifier le tracé des rues.

Nous utilisons comme base de données cartographique [OpenStreetMap](https://www.openstreetmap.org/), où les tracés des objets (bâtiments, rues, frontières, etc.) sont décrits par une série de points, ou *polyligne*.

En travaillant avec les classes de Sébastien Hamon et Philippe Lac, nous espérons proposer des algorithmes de simplification de tracé qui correspondent aux besoins des déficients visuels.

## Description du problème

D'un point de vue informatique, le problème que l'on se propose de résoudre peut se décrire ainsi: 

* *En entrée du programme:* une série de points (donnés par leurs coordonnées 2D) décrivant une route, ou un réseau de routes.
* *En entrée du programme:* une série de points décrivant la même information géométrique de manière simplifiée.

*Objectif de l'algorithme:* produire un tracé semblable au tracé initial, mais qui ne comporte plus les détails inutiles à la compréhension du tracé pour un public déficient visuel.

Lors de notre discussion du 5 février 2018, nous avions étudié un exemple de polyligne, et discuté de la manière de décider si un point devait être conservé ou non. Nous avons notamment dit que:

* le premier et le dernier point devaient être conservés.
* les points qui représentaient un virage important devaient être conservés.
* si supprimer un point modifie peu le tracé global (c'est-à-dire si le point supprimé est proche du nouveau segment créé), alors cette suppression n'est pas problématique
* si l'angle formé par les deux segments adjacents à un point était très plat, on pouvait supprimer ce point.

![Exemple de polyligne](illustrations/trace-exemple.png)

Cependant, ces premières réflexions pourraient être remises en cause par les expérimentations que vous réaliserez au fil du projet.

## Méthode de validation

L'équipe d'ACTIVmap travaille en ce moment avec [DV-Auvergne](http://dv-auvergne.org/) à la conception de cartes des quartiers de Jaude et de Gaillard à Clermont-Ferrand. Ces cartes en relief seront installées auprès des arrêts de tramway, et constitueront la première réalisation commune.

Dans ce contexte, l'équipe d'ACTIVmap et DV-Auvergne recueilleront régulièrement des tracés de rues aux formes complexes, qu'il faudra être capable de simplifier.

On se propose donc d'échanger avec les élèves du lycée des données extraites d'OpenStreetMap, et correspondant à ces formes complexes, afin qu'ils les confrontent à leurs programmes.

## Valorisation du travail réalisé

À l'occasion du rassemblement des élèves d'ICN Campus, le jeudi 3 mai 2018 à l'Université Clermont Auvergne, chaque classe présentera ses travaux, sous forme d'un
stand de démonstration.

Dans ce contexte, il est important de pouvoir expliquer les enjeux et défis liés au problème qui nous a intéressé. Il pourrait par exemple être intéressant d'avoir un support (poster, affiche, ou encore présentation à projeter) qui raconte ce qu'est la déficience visuelle, ce qu'est OpenStreetMap, et quels sont les défis pour rendre accessibles les cartes aux déficients visuels.

Il sera aussi intéressant de pouvoir présenter des résultats de vos algorithmes, et d'expliquer votre démarche scientifique.

## Proposition de cahier des charges

On se propose de décomposer le problème initial en plusieurs objectifs, certains indépendants, d'autres nécessitant la réalisation des objectifs précédents. En voici une liste partiellement ordonnée, qui pourra évoluer au fil du temps, et de nos discussions.

* écrire un algorithme qui prend une polyligne de 4 points en entrée, et retourne une polyligne simplifiée, où certains point auront été supprimés.
* écrire un algorithme qui prend une polyligne plus longue, et qui fait le même travail de simplification. On pourra par exemple imaginer une approche itérative.
* écrire un programme qui puisse lire un fichier décrivant une polyligne, dans un format convenu avec l'équipe d'ACTIVmap.
* concevoir des exemples de tracés qui pourraient être problématiques pour les algorithmes, en s'inspirant des tracés de rues présents dans OpenStreetMap.
* tester les différents algorithmes sur des données issues d'OpenStreetMap.
* réfléchir à la manière de prendre en compte non plus une polyligne, mais plusieurs polylignes qui décrivent deux rues qui se croisent.

## Organisation par méthodes agiles

Afin de faciliter le suivi de chacune des équipes par les enseignants, nous avons discuté lors de ma visite à Moulins de l'utilisation des méthodes agiles.

Le cahier des charges est décomposé en [objectifs élémentaires](http://fc.isima.fr/~favreau/gestion-de-projets/methodes-agiles.html#/6/5) (on appelle cela le *backlog*), puis au début de chaque session de travail (ou sprint), on décompose ces objectifs en tâches simples. On utilise alors un [kanban](http://fc.isima.fr/~favreau/gestion-de-projets/methodes-agiles.html#/5/2), pour représenter par un post-it les tâches élémentaires. On peut ainsi visualiser les tâches en cours, et les élèves qui les ont prises en charge.

## Données disponibles

Plusieurs tracés de rues complexes ont été extraits d'OpenStreetMap grâce au logiciel [JOSM](http://josm.openstreetmap.de/), puis convertis en un format texte grâce à un [script python](scripts/convert.py):

* [Rue de l'Ange;Rue des Vieillards.txt](donnees/Rue de l'Ange;Rue des Vieillards.txt)
* [Rue Fontgiève.txt](donnees/Rue Fontgiève.txt)
* [Rue Sainte-Rose.txt](donnees/Rue Sainte-Rose.txt)

