#!/usr/bin/python2.7 
# coding: utf8

import sys
import os
from imposm.parser import OSMParser

# on vérifie qu'il y a bien un fichier en paramètre
if len(sys.argv) != 3:
  print("./convert.py input.osm repertoire")
  exit(1)

fnom = sys.argv[1]
repertoire = sys.argv[2]

noeuds = {}

def stocker_coords(coords):
  for osm_id, lon, lat in coords:
    noeuds[osm_id] = [lon, lat]

chemins = {}

def stocker_rues(rues):
  for rue in rues:
    print "on lit la rue", rue[1]["name"]
    chemins[rue[1]["name"]] = rue[2]

print "Chargement de la donnees OSM", fnom
p = OSMParser(concurrency=4, coords_callback=stocker_coords, ways_callback=stocker_rues)
p.parse(fnom)

for c in chemins:
  fichierchemin = repertoire + os.path.sep + c + ".txt"
  print "Ecriture du fichier", fichierchemin
  F = open(fichierchemin, "w")
  for p in chemins[c]:
    F.write(str(noeuds[p][0]) + " " + str(noeuds[p][1]) + "\n")
  F.close()
